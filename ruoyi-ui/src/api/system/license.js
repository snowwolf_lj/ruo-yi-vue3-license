import request from '@/utils/request'

// 查询菜单列表
export function generateLicense(data) {
    return request({
        url: '/license/generate',
        method: 'post',
        data: data
    })
}
