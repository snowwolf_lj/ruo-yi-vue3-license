package com.ruoyi.web.controller.license;

import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.license.domain.LicenseCreatorParam;
import com.ruoyi.license.service.AbstractServerInfos;
import com.ruoyi.license.service.impl.LinuxServerInfos;
import com.ruoyi.license.service.impl.WindowsServerInfos;
import com.ruoyi.license.utils.LicenseCreator;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.*;


/**
 * 用于生成证书文件，不能放在给客户部署的代码里
 */
@RestController
@RequestMapping("/license")
public class LicenseController {

    /**
     * 获取服务器硬件信息
     */
    @GetMapping(value = "/getInfo")
    public AjaxResult getInfo(@RequestParam(value = "osName",required = false) String osName) {
        //操作系统类型
        if(StringUtils.isBlank(osName)){
            osName = System.getProperty("os.name");
        }
        osName = osName.toLowerCase();
        AbstractServerInfos abstractServerInfos = null;
        //根据不同操作系统类型选择不同的数据获取方法
        if (osName.startsWith("windows")) {
            abstractServerInfos = new WindowsServerInfos();
        } else if (osName.startsWith("linux")) {
            abstractServerInfos = new LinuxServerInfos();
        }else{//其他服务器类型
            abstractServerInfos = new LinuxServerInfos();
        }
        return AjaxResult.success(abstractServerInfos.getServerInfos());
    }

    @PostMapping(value = "/generate")
    public AjaxResult generate(@RequestBody LicenseCreatorParam param) {
        LicenseCreator licenseCreator = new LicenseCreator(param);
        boolean result = licenseCreator.generateLicense();
        if(result){
            return AjaxResult.success();
        }else{
            return AjaxResult.error("证书文件生成失败");
        }
    }
}
